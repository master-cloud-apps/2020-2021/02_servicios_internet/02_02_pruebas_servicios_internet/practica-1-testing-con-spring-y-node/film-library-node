const createTable = require('./../../src/db/createTable.js')
const { setDynamoDBMockImplementation } = require('./index.js')

jest.mock('aws-sdk')

const tableSportsName = 'sports'

describe('createTable use cases', () => {
  test('When call to create table should dynamoDB createTable be called', () => {
    setDynamoDBMockImplementation(() => Promise.resolve(`Table ${tableSportsName} created`))
    return createTable(tableSportsName)
      .then(response => expect(response).toBe(`Table ${tableSportsName} created`))
  })
  test('Given createTable return exception When call to create table should dynamoDB createTable be called', () => {
    setDynamoDBMockImplementation(() => Promise.reject(new Error('Cannot create preexisting table')))
    return createTable(tableSportsName)
      .then(response => expect(response).toBeUndefined())
  })
  test('Given createTable return exception When call to create table should dynamoDB createTable be called', () => {
    setDynamoDBMockImplementation(() => Promise.reject(new Error('DB not available')))
    return createTable(tableSportsName)
      .then(response => expect(response).toBeUndefined())
  })
})
