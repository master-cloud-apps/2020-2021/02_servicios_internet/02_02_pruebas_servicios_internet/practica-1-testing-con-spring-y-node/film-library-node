const badTableNameMessage = {
  message: 'Bad TableName'
}
const badRequest = {
  message: 'Bad Item title'
}

const dynamoFake = (() => {
  const elements = { films: [] }
  return {
    scan: (params, callback) => {
      if (params.TableName === undefined || !(params.TableName in elements)) {
        return callback(badTableNameMessage, null)
      }
      return callback(null, { Items: elements[params.TableName] })
    },
    put: (params, callback) => {
      if (params.Item.title === undefined) {
        return callback(badRequest, null)
      }
      elements[params.TableName].push(params.Item)
      return callback(null, { Item: params.Item })
    },
    putAsync: (params) => {
      if (params.TableName === undefined || !(params.TableName in elements)) {
        return Promise.reject(badTableNameMessage)
      }
      elements[params.TableName].push(params.Item)
      return Promise.resolve({ Item: params.Item })
    },
    deleteAsync: (params) => {
      elements[params.TableName] = elements[params.TableName].filter(element => element.id !== params.id)
      return Promise.resolve({ id: params.id })
    }
  }
})()

module.exports = dynamoFake
